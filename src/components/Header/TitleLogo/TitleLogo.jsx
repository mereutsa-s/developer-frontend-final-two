import { Link } from "react-router-dom";
import "./TitleLogo.css";

function TitleLogo() {
    return (
        <Link to={"/"} id={"page_start"}  className="titlle-logo">
            <img className="titlle-logo__img" src="../resources/favicon.svg" alt="Логотип магазина" />
            <h1 className="titlle-logo__text"><span className="titlle-logo__word">Мой</span>Маркет</h1>
        </Link>
    )
}

export default TitleLogo;