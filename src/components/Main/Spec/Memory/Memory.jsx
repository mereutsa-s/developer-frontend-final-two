import React, { useState } from "react";
import product from "../../../../data/data";
import styles from "./Memory.module.css";

function Memory() {
    const [selectMemory, setFunc] = useState("Не выбрано");

    return (
        <div className={styles.memory}>
            <h3 className={styles.memory__header}>Конфигурация товара: {selectMemory}</h3>

            <div className={styles.memory__btnBox}>
                { product.select_memory.map((mem) => {
                    let select = (mem.Size === selectMemory) ? `${styles.btnMemory_selectItem_color}` : ``;
                    return (
                    <button className={`${styles.btnMemory} ${select}`}
                    key={mem.id}
                    onClick={() => {setFunc(mem.Size);}}
                    >{mem.Size}</button>
                    )
                })}
            </div>
        </div>
    );
};

export default Memory;