import { createSlice } from "@reduxjs/toolkit";

localStorage.getItem(`cart`) === null && localStorage.setItem(`cart`, JSON.stringify( [] )); //инициализация ключа в локалсторейдж

export const cartSlice = createSlice({
    name: "cart",

    initialState: {
        products: JSON.parse(localStorage.getItem(`cart`)),  // в дефолт принимает локал
    },

    reducers: {
        addProduct: (State, action) => {  // при первом нажатии кнопки
            return {
                ...State, products: [...State.products, action.payload],
            };
        },

        delProduct: (State, action) => { // при втором нажатии кнопки
            return {
                ...State, products: State.products.filter((elem) => elem !== action.payload),
            }
        },
    },
});

export const { addProduct, delProduct } = cartSlice.actions;

export default cartSlice.reducer;