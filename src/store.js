import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./reducer/cartReducer";
import likeReduser from "./reducer/likeReduser";

let n = 0;
const logger = (store) => (next) => (action) => {
    // console.log("action", action.payload);
    let result = next(action);
    // console.log("next state", store.getState().cart.products);
    console.log(`Количество обработанных действий: ${++n}`);
    return result;
};

const sideCart = (store) => (next) => (action) => {
    let result = next(action);
    if (action.type === `cart/addProduct` || action.type === `cart/delProduct`) { 
        localStorage.setItem(`cart`, JSON.stringify(store.getState().cart.products)); 
    };
    return result;
};

const sideLike = (store) => (next) => (action) => {
    let result = next(action);
    if (action.type === `like/addLike` || action.type === `like/delLike`) {
        localStorage.setItem(`like`, JSON.stringify(store.getState().like.products)); 
    };
    return result;
};

export const store = configureStore({
    reducer: {
        cart: cartReducer,
        like: likeReduser,
    },
    middleware: [logger, sideCart, sideLike],
});
