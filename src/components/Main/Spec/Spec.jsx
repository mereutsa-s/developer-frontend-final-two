import React from "react";
import Color from "./Color/Color";
import Memory from "./Memory/Memory";
import Feature from "./Feature/Feature";
import Description from "./Description/Description";
import Table from "./Table/Table";
import Buy from "./Buy/Buy"
import Fraim from "./Fraim/Fraim";
import product from "../../../data/data";
import "./Spec.css";

function Spec() {
    return (
        <section className="spec-and-bar">
            <div className="spec-all spec-all_header_font">
                <Color />
                <Memory />
                <Feature 
                    screen={product.feature.screen}
                    builtin_memory={product.feature.builtin_memory}
                    operating_system={product.feature.operating_system}
                    wireless_interfaces={product.feature.wireless_interfaces}
                    processor={product.feature.processor}
                    weight={product.feature.weight}
                />
                <Description 
                    chart1={product.description.par_1}
                    chart2={product.description.par_2}
                />
                <Table />
            </div>
            <aside className="bar">
                <Buy
                    price={product.price.price_new}
                    discount={product.price.discount}
                    delivery={product.delivery}
                    product={product.name}
                />
                <Fraim />
            </aside>
        </section>
    );
};

export default Spec;