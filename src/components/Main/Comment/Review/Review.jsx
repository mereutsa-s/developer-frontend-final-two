import React from "react";
import "./Review.css";


function Review({comment}) {
    let stars = [];
    stars.length = 5;
    stars.fill(0);

    return (
        <div className="review">
            <img className="review__photo" src={comment.photo} alt={comment.alt_photo} />

            <div className="review__text">
                <div className="review__header">
                    <h4 className="review__name">{comment.name}</h4>

                    <div className="review__star">
                        {stars.map((star, index) => {
                            if (index < comment.grade) {
                            return <img src="./resources/star-gold.png" alt="Светлая звезда" key={index}/>
                            } else {
                            return <img src="./resources/star-black.png" alt="Темная звезда" key={index}/>
                            }
                        })}
                    </div>
                </div>

                <div className="review__experience">
                    <div className="review__text-part">
                        <p><b>Опыт использования:</b>{comment.experience}</p>
                    </div>

                    <div className="review__text-part">
                        <p><b>Достоинства:</b><br />{comment.dignity}</p>
                    </div>

                    <div className="review__text-part">
                        <p><b>Недостатки:</b><br />{comment.limitations}</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Review;