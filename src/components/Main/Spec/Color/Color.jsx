import React, { useState } from "react";
import product from "../../../../data/data";
import styled from "styled-components";
const Box = styled.div`
    display: flex;
    gap: 10px;
    @media (max-width: 1024px){
        flex-wrap: wrap;
    }
    @media (max-width: 360px){
        gap: 7px;

    }
`;
const Image = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: stretch;
    padding: 10px;
    gap: 10px;

    width: 80px;
    height: 80px;

    border: ${(props) => props.selected ? `2px solid #F36223` :  `1px solid #888888`};
    border-radius: 4px;
        &:hover {
            cursor: pointer;
            background-color: #F2F2F2;
        }
        &:active {
            border: 2px solid #A93A09;
            background-color: #F8FAFE;
        }
        @media (max-width: 360px) {
            width: 60px;
            height: 60px;
        }
`;

function Color() {
    const [selectColor, setFunc] = useState("Не выбрано");

    return (
        <div className="color-panel">
            <h3 className="color-panel__header">Цвет товара: {selectColor}</h3>

            <Box>
                { product.view.map((img, ind) => {
                    return (
                        <Image 
                            selected={img.color === selectColor}
                            key={ind} 
                            onClick={() => {
                                setFunc(img.color);
                            }}>
                            <img src={img.src} alt={img.alt} />
                        </Image>
                    );
                })}
            </Box>
        </div>
    );
};

export default Color;