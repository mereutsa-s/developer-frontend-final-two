import { Link } from "react-router-dom";
import "./MainIndex.css";

function MainIndex() {
    return (
        <div className="main-index">
            <p className="main-index__text">
                Здесь должно быть содержимое<br className="main-index__breck2"/> главной страницы. <br className="main-index__breck"/>
                Но в рамках курса главная<br className="main-index__breck2"/> страница  используется лишь <br />
                в демонстрационных целях
            </p>
            <Link to={"/product"}>Перейти на страницу товара</Link>
        </div>
    )
}

export default MainIndex;