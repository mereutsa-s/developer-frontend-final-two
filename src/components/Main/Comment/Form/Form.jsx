import { useState } from "react";
import "./Form.css";

function Form() {
    /* /====================================/ */
    // состоние значений импутов
    /* /====================================/ */
    const [ inputName, setInputName ] = useState(localStorage.getItem(`nameImput`) ? localStorage.getItem(`nameImput`) : ``);
    const [ inputGrade, setInputGrade ] = useState(localStorage.getItem(`gradeImput`) ? localStorage.getItem(`gradeImput`) : ``);
    const [ inputTextArea, setInputTextArea ] = useState(localStorage.getItem(`textImput`) ? localStorage.getItem(`textImput`) : ``);
    
    /* /====================================/ */
    // состояние ошибки
    /* /====================================/ */
    const [validationNameError, setValidationNameError] = useState("");
    const [validationGradeError, setValidationGradeError] = useState("");

    /* /====================================/ */
    // привязка импутов
    /* /====================================/ */
    const handleInputName = (e) => {
        setValidationNameError("");
        setInputName(e.target.value);
        localStorage.setItem(`nameImput`, e.target.value);
    };
    const handleInputGrade = (e) => {
        setValidationGradeError("");
        setInputGrade(e.target.value);
        localStorage.setItem(`gradeImput`, e.target.value);
    };
    const handleInputTextArea = (e) => {
        setInputTextArea(e.target.value);
        localStorage.setItem(`textImput`, e.target.value);
    };

    const nameValidation = () => {
        if (!inputName) {
            setValidationNameError("Вы забыли указать имя и фамилию");
        } else if (inputName.length <= 2) {
            setValidationNameError("Имя не может быть короче 2-х символов");
        } else {
            return true;
        }
    };
    
    const gradeValidation = () => {
        if (inputGrade > 6 || inputGrade < 1 || isNaN(inputGrade)) {
            setValidationGradeError("Оценка должна быть от 1 до 5");
        } else {
            return true;
        }
    };
    
    const handleSubmit = (e) => {
        e.preventDefault();
        nameValidation();
        gradeValidation();
        if (nameValidation() && gradeValidation()) {
            alert(`Ваш отзыв был успешно отправлен и будет отображён после модерации`);
            localStorage.removeItem(`nameImput`);
            localStorage.removeItem(`gradeImput`);
            localStorage.removeItem(`textImput`);

            setInputName(``);
            setInputGrade(``);
            setInputTextArea(``);
        };
    };

    return (
        <form action="#" className="form comments__form" onSubmit={handleSubmit}>
            <fieldset className="form__fildset">
                <legend className="form__header">Добавить свой отзыв</legend>

                <div className="form__imput">
                    <div className="input-wrapper">
                        <input className={`input-wrapper__input 
                            ${validationNameError && "input-wrapper__input_border_color"}`} 
                            type="text" id="name" name="name" placeholder="Имя и фамилия" 
                            onInput={handleInputName} value={inputName}>
                        </input>
                        
                        {validationNameError && (
                        <div className={`input-wrapper__valid`}>
                            <p>{validationNameError}</p>
                        </div>
                        )}
                    </div>

                    <div className="input-wrapper input-wrapper_fix-flex">
                        <input className={`input-wrapper__input-grade 
                            ${validationGradeError && "input-wrapper__input_border_color"}`}
                            type="text" id="grade" name="grade" placeholder="Оценка" 
                            onInput={handleInputGrade} value={inputGrade}>
                        </input>
                        
                        {validationGradeError && (
                        <div className={`input-wrapper__valid`}>
                            <p>{validationGradeError}</p>
                        </div>
                        )}
                    </div>
                </div>         

                <textarea className="form__text" id="text" 
                    name="text" placeholder="Текст отзыва" 
                    onInput={handleInputTextArea} value={inputTextArea}>
                </textarea>

                <div className="form__box-btn">
                    <input className="form__btn" type="submit" value="Отправить отзыв"></input>
                </div>
            </fieldset>
        </form>
    );
 };

 export default Form;