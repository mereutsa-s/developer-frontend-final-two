import React from "react";
import product from "../../../data/data";
import "./ProdTitle.css";

function ProdTitle() {
    return (
        <section className="product-title">
            <div className="product-header product-title__name">
                <h2 className="product-header__header">{product.name}</h2>
            </div>

            <div className="product-images product-title__images">
                {product.product_photo_link.map((el, ind) => {
                    return <img key={ind} src={el.link} alt={el.alt} />
                })}
            </div>
        </section>
    );
};

export default ProdTitle;