import { useCurrentDate } from "@kundinos/react-hooks";
import "./Footer.css";

function Footer() {
    const currentDate = useCurrentDate();
    const fullYear = currentDate.getFullYear();

    return (
        <footer className="footer page__footer">
            <div className="footer__box  container">
                <div className="footer__info">
                    <p><b>© ООО «<span className="footer__word">Мой</span>Маркет», 2018 - {`${fullYear}`}.</b><br />
                        Для уточнения информации звоните по номеру 
                        <a href={"tel:+79000000000"}>{` +7 900 000 0000`}</a>,
                        <br className="footer__br" />
                        а предложения по сотрудничеству отправляйте на почту
                        <a href={"mailto:partner@mymarket.com"}>{` partner@mymarket.com`}</a>
                    </p>
                </div>

                <div className="footer__anchor">
                    <a href={"#page_start"}>{`Наверх`}</a>
                </div>
            </div>
        </footer>
    )
};

export default Footer;