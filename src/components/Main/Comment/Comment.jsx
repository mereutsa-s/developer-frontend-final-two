import React from "react";
import Form from "./Form/Form";
import Review from "./Review/Review";
import comments from "../../../data/Comments";
import "./Comment.css";

function Comment() {
    return (
        <section className="comments item__comments">
            <div className="comments__head">
                <h3 className="comments__header">Отзывы</h3>
                <span className="comments__counter">{comments.length}</span>
            </div>
            
            {comments.map((user, index) => {
                return (
                    <div className="comments__list" key={user.id}>
                        <Review comment={user}/>
                        {(index < (comments.length - 1)) ? <div className="separator"></div> : null}
                    </div>
                )
            })}           

            <Form />
        </section>
    );
};

export default Comment;