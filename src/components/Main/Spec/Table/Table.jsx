import React from "react";
import "./Table.css";

function Table() {
    return (
        <div className="compare">
            <h3 className="compare__header">Сравнение моделей</h3>

            <table className="table">
                <thead>
                    <tr className="table__head-row">
                        <th className="table__head-data">Модель</th>
                        <th className="table__head-data">Вес</th>
                        <th className="table__head-data">Высота</th>
                        <th className="table__head-data">Ширина</th>
                        <th className="table__head-data">Толщина</th>
                        <th className="table__head-data">Чип</th>
                        <th className="table__head-data">Объём памяти</th>
                        <th className="table__head-data">Аккумулятор</th>
                    </tr>
                </thead>

                <tbody>
                    <tr className="table__row">
                        <td className="table__data">Iphone 11</td>
                        <td className="table__data">194 грамма</td>
                        <td className="table__data">150.9 мм</td>
                        <td className="table__data">75.7 мм</td>
                        <td className="table__data">8.3 мм</td>
                        <td className="table__data">A13 Bionic chip</td>
                        <td className="table__data">до 128 Гб</td>
                        <td className="table__data">До 17 часов</td>
                    </tr>

                    <tr className="table__row">
                        <td className="table__data">Iphone 12</td>
                        <td className="table__data">164 грамма</td>
                        <td className="table__data">146.7 мм</td>
                        <td className="table__data">71.5 мм</td>
                        <td className="table__data">7.4 мм</td>
                        <td className="table__data">A14 Bionic chip</td>
                        <td className="table__data">до 256 Гб</td>
                        <td className="table__data">До 19 часов</td>
                    </tr>

                    <tr className="table__row">
                        <td className="table__data">Iphone 13</td>
                        <td className="table__data">174 грамма</td>
                        <td className="table__data">146.7 мм</td>
                        <td className="table__data">71.5 мм</td>
                        <td className="table__data">7.65 мм</td>
                        <td className="table__data">A15 Bionic chip</td>
                        <td className="table__data">до 512 Гб</td>
                        <td className="table__data">До 19 часов</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Table;