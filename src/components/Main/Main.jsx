import React from "react";
import Navigation from './Navigation/Navigation.jsx';
import ProdTitle from './ProdTitle/ProdTitle.jsx';
import Spec from './Spec/Spec.jsx';
import Comment from './Comment/Comment.jsx';
import navi from "../../data/Navigation";

function Main() {
    return (
        <main className="main container">
            <Navigation link={navi}/>
            <article className="item">
                <ProdTitle />
                <Spec />
                <Comment />
            </article>
        </main>
    );
};

export default Main;