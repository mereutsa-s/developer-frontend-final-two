import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import PageProduct from "./components/PageProduct/PageProduct";
import PageIndex from "./components/PageIndex/PageIndex";
// import PageTest from './components/PageTest/PageTest';
import './App.css';

function App() {
	return (
		<BrowserRouter>
			<Routes className="App">
				<Route path="/" element={<PageIndex />} />

				<Route path="/product" element={<PageProduct />} />

				{/* <Route path="/test" element={<PageTest />} /> */}

				{/* <Route path="*" element={<PageNotFound />} /> */}
			</Routes>
		</BrowserRouter>
	);
}

export default App;
