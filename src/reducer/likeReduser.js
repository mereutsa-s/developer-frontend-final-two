import { createSlice } from "@reduxjs/toolkit";

localStorage.getItem(`like`) === null && localStorage.setItem(`like`, JSON.stringify( [] )); //инициализация ключа в локалсторейдж

export const likeSlice = createSlice({
    name: "like",

    initialState: {
        products: JSON.parse(localStorage.getItem(`like`)),  // в дефолт принимает локал
    },

    reducers: {
        addLike: (State, action) => {  // при первом нажатии кнопки
            return {
                ...State, products: [...State.products, action.payload],
            };
        },

        delLike: (State, action) => { // при втором нажатии кнопки
            return {
                ...State, products: State.products.filter((elem) => elem !== action.payload),
            }
        },
    },
});

export const { addLike, delLike } = likeSlice.actions;

export default likeSlice.reducer;