import React from "react";
import "./Description.css";

function Description({chart1, chart2}) {
    return (
        <div className="description">
            <h3 className="description__header">Описание</h3>

            <div className="description__text" >
                <div className="description__text-chart1" dangerouslySetInnerHTML={{ __html: chart1 }}></div>
                <div className="description__text-chart2" dangerouslySetInnerHTML={{ __html: chart2 }}></div>
            </div>
        </div>
    );
}

export default Description;