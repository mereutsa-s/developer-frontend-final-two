import React from "react";
import "./Fraim.css";

function Fraim() {
    return (
        <div className="fraim">
            <p className="fraim__headr">Реклама</p>
            <div className="fraim__box">
                {/* <iframe title="iFrame" className="ad" src="../resources/color-4.webp"></iframe> */}
                <iframe title="iFrame" className="ad" src="../ads/ads.html"></iframe>
            </div>
        </div>
    );
};

export default Fraim;