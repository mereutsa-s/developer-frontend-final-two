// import { Link } from "react-router-dom";
import "./Feature.css";

function Feature({screen, builtin_memory, operating_system, wireless_interfaces, processor, weight}) {
    return (
        <div className="feature">
            <h3 className="feature__header">Характеристики товара</h3>

            <ul className="feature__list">
                <li className="feature__list-item ">Экран: <b>{screen}</b></li>
                <li className="feature__list-item">Встроенная память: <b>{builtin_memory}</b></li>
                <li className="feature__list-item">Операционная система: <b>{operating_system}</b></li>
                <li className="feature__list-item">Беспроводные интерфейсы: <b>{wireless_interfaces.map((el, ind) => <span key={ind}>{el}</span>)}</b></li>
                <li className="feature__list-item">Процессор: 
                <a href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank" rel="noreferrer">{processor}</a>
                </li>
                <li className="feature__list-item">Вес: <b>{weight}</b></li>
            </ul>
        </div>
    );
};

export default Feature;