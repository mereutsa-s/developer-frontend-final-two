import Header from "../Header/Header";
import Footer from "../Footer/Footer"; 
import MainIndex from "../Main/MainIndex/MainIndex";

function PageIndex() {
    return (
        <div className="page">
            <Header />
            <MainIndex />
            <Footer />
        </div>
    )
}

export default PageIndex;