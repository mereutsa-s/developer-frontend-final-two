import { Link } from "react-router-dom";
import "./Navigation.css";

function Navigation({ link }) {
    return (
        <nav className="navigation page__navigation">
            {link.map((el, index) => {
                return (
                    <div className="navigation__link" key={el.id}>
                        <Link to={el.link}>{el.name}</Link>
                        {(index < (link.length - 1)) ? <span>{`>`}</span> : null}
                    </div>
                )
            })}
        </nav>
    );
};

export default Navigation;