const navi = [
    {
        id: 1,
        name: "Главная страница",
        link: "/",
    },
    {
        id: 2,
        name: "Смартфоны и гаджеты",
        link: "#",
    },
    {
        id: 3,
        name: "Мобильные телефоны",
        link: "/test",
    },
    {
        id: 4,
        name: "Apple",
        link: "/product",
    },

]
export default navi;